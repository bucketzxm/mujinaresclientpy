# -*- coding: utf-8 -*-

import typing
import threading
import time
from mujinplc import plcmemory, plccontroller, plclogic, plcproductionrunner
from mujinplc import PLCDataObject

try:
    from mujincontrollerclient.realtimerobotclient import RealtimeRobotControllerClient
    from mujincontrollerclient.controllerclientraw import ControllerWebClient
    from mujincontrollerclient import controllerclientbase
except ImportError as e:
    log.info("mujincontrollerclientpy not installed. you aren't able to use visionsimulation function")

from .aresmessage import Message, MessageType
from .aressocket import Socket

import logging
log = logging.getLogger(__name__)

class Task(PLCDataObject):
    """
    Ares Task info . This is used to communicate between arec client and ares rcs.
    """
    actionID = 0 # type: int
    armID = 0 # type: int
    partType = '' # type: str
    orderNumber = 0 # type: int

    # pick location info
    pickLocationReady = 0 # type: int # 0 is ready , 1 is not ready .
    pickLocationIndex = 0 # type: int
    pickContainerId = '' # type: str
    pickContainerType = '' # type: str

    # place location info
    placeLocationReady = 0 # type: int # 0 is ready , 1 is not ready .
    placeLocationIndex = 0 # type: int
    placeContainerId = '' # type: str
    placeContainerType = '' # type: str

    # packing
    packInputPartIndex = 0 # type: int
    packFormationComputationName = '' # type: str

class Client(plcproductionrunner.PLCMaterialHandler):
    _memory = None # type:plcmemory.PLCMemory
    _productionRunner = None  # type: plcproductionrunner.PLCProductionRunner
    _socket = None # type: Socket

    _workerThread = None # type: typing.Optional[threading.Thread]
    _reportThread = None # type: typing.Optional[threading.Thread]

    _locationLocks = None # type: typing.Dict[int, threading.Lock]
    _locationConditions = None # type: typing.Dict[int, threading.Condition]
    _locationContainers = None # type: typing.Dict[int, typing.Tuple[str, str]] # a map to reflect real world location-container mapping
    _maxLocationIndex = 4

    _tasksQueued = None # type: typing.Set[int] # a set to make uniqueness of task in order queue
    _isok = False # type: bool
    _timeout = 1.0 # type: float

    _warehouse = '1' # type: str
    _deviceType = 1 # type: int, for arm should always be 1
    _rid = 1 # type: int

    _config = {}  # user input configuration
    _registered = 0 # type: float
    _useVisionSimulation = False

    def __init__(self, memory: plcmemory.PLCMemory, endpoint: typing.Tuple[str, int], maxLocationIndex: int = 4, timeout: float = 10.0, useVisionSimulation=False, **kwargs):
        self._productionRunner = plcproductionrunner.PLCProductionRunner(memory, self, maxLocationIndex=maxLocationIndex)
        self._memory = memory
        self._socket = Socket(endpoint)
        self._timeout = timeout
        self._warehouse = kwargs.get('warehouseID', '')
        self._armID = int(kwargs.get('armID', 0))
        self._deviceType = kwargs.get('deviceType', 1)
        self._config = kwargs.copy()
        self._rid = self._config.get('armID', 1)
        self._useVisionSimulation = useVisionSimulation
        self._locationLocks = {}
        self._locationConditions = {}
        self._locationContainers = {}
        self._maxLocationIndex = maxLocationIndex
        for i in range(1, maxLocationIndex + 1):
            self._locationContainers[i] = ('', '')
            self._locationLocks[i] = threading.Lock()
            self._locationConditions[i] = threading.Condition(self._locationLocks[i])
        self._tasksQueued = set()

    async def MoveLocationAsync(self, locationIndex: int, expectedContainerId: str, expectedContainerType: str, orderUniqueId: str) -> typing.Tuple[str, str]:
        log.warn('move location %d: expectedContainerId = %r, expectedContainerType = %r, orderUniqueId = %r', locationIndex, expectedContainerId, expectedContainerType, orderUniqueId)

        with self._locationLocks[locationIndex]:
            locationContainerId, locationContainerType = self._locationContainers[locationIndex]
            if locationContainerId == expectedContainerId and locationContainerType == expectedContainerType:
                return locationContainerId, locationContainerType

            # we will move container away
            self._locationContainers[locationIndex] = ('', '')

        # need to move the existing container away
        response = self._socket.SendRequest(Message({
            'type': 'resourceRelease',
            'warehouseID': self._warehouse,
            'deviceType': self._deviceType,
            'params': {
                'armID': self._rid,
                'locationIndex': locationIndex,
            }
        }), retry=3, timeout=self._timeout)
        if response is None or self._HasError(response):
            raise Exception('did not get a response from server')
        # TODO(simon): deal with response

        # wait until the condition is met
        with self._locationLocks[locationIndex]:
            locationContainerId, locationContainerType = self._locationContainers[locationIndex]
            while locationContainerId != expectedContainerId or locationContainerType != expectedContainerType:
                locationContainerId, locationContainerType = self._locationContainers[locationIndex]
                self._locationConditions[locationIndex].wait(0.1)
            return locationContainerId, locationContainerType

    async def FinishOrderAsync(self, orderUniqueId: str, orderCycleFinishCode: plclogic.PLCOrderCycleFinishCode, numPutInDestination: int) -> None:
        log.warn('finish order: orderUniqueId = %r, orderCycleFinishCode = %r, numPutInDestination = %r', orderUniqueId, orderCycleFinishCode, numPutInDestination)

        # remove actionID from set of queued tasks
        actionID = int(orderUniqueId)
        if actionID in self._tasksQueued:
            self._tasksQueued.remove(actionID)

        # TODO(simon): this will retry indefinitely, decide when to give up
        response = self._socket.SendRequest(Message({
            'type': 'taskFinishReport',
            'warehouseID': self._warehouse,
            'deviceType': self._deviceType,
            'params': {
                'armID': self._rid,
                'actionID': actionID,
                'finishOrderNumber': numPutInDestination,
                'errorCode': 0 if orderCycleFinishCode == plclogic.PLCOrderCycleFinishCode.FinishedOrderComplete else 1,
                'errorReason': int(orderCycleFinishCode),
            },
        }), retry=10, timeout=self._timeout)
        if response is None or self._HasError(response):
            import sys
            sys.exit(1)
            raise Exception('did not get a response from server')
        # TODO(simon): deal with response

    def Start(self):
        self.Stop()

        self._socket.Start()
        self._productionRunner.Start()

        self._isok = True
        self._workerThread = threading.Thread(target=self._RunWorkerThread, name='workerthread')
        self._workerThread.start()

        self._reportThread = threading.Thread(target=self._ReportThread, name='reportthread')
        #self._reportThread.start()

        if self._useVisionSimulation:
            self._visionsimulationthread = threading.Thread(target=self._RunVisionSimulationThread, name='visionsimulationthread')
            self._visionsimulationthread.start()

    def Stop(self):
        self._isok = False
        self._productionRunner.Stop()
        self._socket.Stop()

        if self._workerThread is not None:
            self._workerThread.join()
            self._workerThread = None

        if self._reportThread is not None:
            self._reportThread.join()
            self._reportThread = None

    def _RunVisionSimulationThread(self):
        """ simulation thread
        this function will  loop and check current containerId in each location
        1. keep a last known mapping of container id type of each location
        2. sleep and check if anything is different, record when it changed, then when some number of seconds passed (detection time), call mujincontrollerclient UpdateObject for that location

        """
        self._WaitUntilConnected()
        log.error('VisionSimulationthread started')
        detectionDelay = self._config.get('detectionDelay', 4) # seconds
        controllerurl = self._config.get('controllerurl', '')
        controllerusername = self._config.get('controllerusername', '')
        controllerpassword = self._config.get('controllerpassword', '')

        webclient = ControllerWebClient(controllerurl, controllerusername, controllerpassword)
        webclient.Login()
        response = webclient.Request('GET', '/config/')
        binpickingconf = response.json()
        robotname = binpickingconf.get('robotname', '')
        robots = binpickingconf.get('robots', {})
        devices = binpickingconf.get('devices', {})
        scenepk = binpickingconf.get('sceneuri', {})
        targetnames = binpickingconf.get('selectedtargetnames', [])
        targetname = ''
        if targetnames:
            targetname = targetnames[0]

        # initialize controllerclient
        controllerclient = RealtimeRobotControllerClient(
            controllerurl = controllerurl,
            controllerusername = controllerusername,
            controllerpassword = controllerpassword,
            robotname = robotname,
            robots = robots,
            devices = devices,
            taskzmqport = None,
            taskheartbeatport = None,
            taskheartbeattimeout = None,
            scenepk = scenepk[len('mujin:/'):],
            tasktype = 'binpicking'
        )
        # envstate = [{'name': 'milk', 'translation_': [0, 750, 900], 'quat_': [1,0,0,0], 'object_uri':'mujin:/milk.mujin.dae'}]
        envstate = self._config.get('envstate', [])
        lastLocationContainer = {}
        for i in range(1, self._maxLocationIndex + 1):
            lastLocationContainer[i] = ('', '')

        #TODO: error handling
        while self._isok:
            for i in range(1, self._maxLocationIndex + 1):
                if self._locationContainers[i] != lastLocationContainer[i]:
                    time.sleep(detectionDelay)
                    # call controllerclient to UpdateObjects
                    response = controllerclient.UpdateObjects(envstate, targetname=targetname)
                    lastLocationContainer[i] = self._locationContainers[i]



    def _RunWorkerThread(self):
        self._WaitUntilConnected()

        while self._isok:
            self._RegisterRobotIfNeeded()
            # deal with requests from server
            request = self._socket.ReceiveRequest(timeout=0.1)
            if not request:
                continue
            try:
                responseParams = {
                    'armID': self._rid,
                    'errorCode': 0,
                    'errorReason': 0,
                }
                if request.body and request.body.get('type') == 'move':
                    try:
                        task = Task(**request.body['params'])
                        responseParams['actionID'] = task.actionID
                        # if task already in self._QueueTask
                        if task.actionID in self._tasksQueued:
                            """
                            errorCode = 1 and errorReason = 99 means task.actionId already in queue. tell server this task has already queueud.
                            """
                            responseParams['errorCode'] = 1
                            responseParams['errorReason'] = 99
                        else:
                            log.info('===============Enqueue Task actionId = %s============='%task.actionID)
                            self._QueueTask(task)
                    except Exception as e:
                        # Queue order failed. return errorcode = 1 and errorreason = 100
                        log.error('task has error = %s'%e)
                        responseParams['errorCode'] = 1
                        responseParams['errorReason'] = 100

                    # update location info from task
                    if task.pickLocationReady == 0:
                        with self._locationLocks[task.pickLocationIndex]:
                            self._locationContainers[task.pickLocationIndex] = (task.pickContainerId, task.pickContainerType)
                        with self._locationConditions[task.pickLocationIndex]:
                            self._locationConditions[task.pickLocationIndex].notify()
                    if task.placeLocationReady == 0:
                        with self._locationLocks[task.placeLocationIndex]:
                            self._locationContainers[task.placeLocationIndex] = (task.placeContainerId, task.placeContainerType)
                        with self._locationConditions[task.placeLocationIndex]:
                            self._locationConditions[task.placeLocationIndex].notify()

                # response to server
                log.error('================== response to ack =====================')
                self._socket.SendResponse(request, Message({
                    'type': request.body['type'],
                    'warehouseID': self._warehouse,
                    'deviceType': self._deviceType,
                    'params': responseParams,
                }))
            except Exception as e:
                log.exception('failed to handle request from server: %s: %r', e, request)

    def _WaitUntilConnected(self) -> None:
        controller = plccontroller.PLCController(self._memory, maxHeartbeatInterval=0.1, heartbeatSignal='commCounter')
        while self._isok:
            if controller.WaitUntilConnected(timeout=0.1):
                log.info('mujin controller connected')
                return

    def _RegisterRobotIfNeeded(self) -> None:
        connected = self._socket.GetConnectedTimestamp()
        if connected == 0:
            # not connected, skip for now
            return
        if self._registered == connected:
            # already registered
            return

        log.debug('registering robot on new connection')
        response = self._socket.SendRequest(Message({
            'type': 'onLine',
            'warehouseID': self._warehouse,
            'deviceType': self._deviceType,
            'params': {
                'armID': self._rid,
            },
        }), timeout=self._timeout)
        if response and not self._HasError(response):
            self._registered = connected
            log.info('register robot succeeded')


    def _ReportThread(self):
        log.info('Report thread started')
        controller = plccontroller.PLCController(self._memory, maxHeartbeatInterval=0.1, heartbeatSignal='commCounter')
        while self._isok:
            actionID = controller.SyncAndGetString("orderUniqueId")
            if actionID:
                actionID = int(actionID)
            else:
                actionID = 0
            response = self._socket.SendRequest(Message({
	                "warehouseID": self._warehouse,
	                "deviceType": self._deviceType,
	                "type": "armStatusReport",
	                "params":{
		                "armID": self._rid,
		                "status": 1 if controller.GetBoolean('isCycleReady') else 0,
		                "seq": int(time.time()*1000),
		                "actionID": actionID,
		                "finishedOrderNumber": controller.GetInteger('numPutInDestination')
	                }
                }), retry=0, timeout=self._timeout, messageType=MessageType.TCP)
            time.sleep(1)

    def _QueueTask(self, task: Task) -> None:
        if task.actionID in self._tasksQueued:
            return
        self._productionRunner.QueueOrder(str(task.actionID), plcproductionrunner.PLCQueueOrderParameters(
            partType = task.partType,
            orderNumber = task.orderNumber,
            pickLocationIndex = task.pickLocationIndex,
            pickContainerId = task.pickContainerId,
            pickContainerType = task.pickContainerType,
            placeLocationIndex = task.placeLocationIndex,
            placeContainerId = task.placeContainerId,
            placeContainerType = task.placeContainerType,
        ))
        self._tasksQueued.add(task.actionID)
        log.info('QueueTask finished ==============================')

    def _HasError(self, message) -> bool:
        params = (message.body or {}).get('params', None) or {}
        errorCode = params.get('errorCode')
        errorReason = params.get('errorReason')
        if errorCode: # errorCode 0 is success
            log.error('error returned from server: errorCode = %r, errorReason = %r', errorCode, errorReason)
            return True
        return False
