# -*- coding: utf-8 -*-
# Definition of message structures

import enum
import struct
import binascii
import json
import typing

import logging
log = logging.getLogger(__name__)

class MessageType(enum.IntEnum):
    """
    max size is 3 bit
    """
    TCP = 0  # normal message. don't need to wait
    RPC = 1  # rpc message, need to wait response
    ACK = 2  # ack message

class CommandType(enum.IntEnum):
    UNKNOWN = 0
    ARM_REGISTER = 1 # arm register message
    JOB_COMPLETE = 2 # order complete
    STATUS_REPORT = 3
    JOB_QUERY = 4
    ARM_UNREGISTER = 5
    ARM_MOVE = 6
    ARM_LOCATION_RELEASE = 7 # move location

COMMAND_TYPE_MAPPING = {
    'unknown': CommandType.UNKNOWN,
    'onLine': CommandType.ARM_REGISTER,
    'move': CommandType.ARM_MOVE,
    'resourceRelease': CommandType.ARM_LOCATION_RELEASE,
    'taskFinishReport': CommandType.JOB_COMPLETE,
    'armStatusReport' : CommandType.STATUS_REPORT
} # type: typing.Dict[str, CommandType]

class Message:
    TYPE = typing.TypeVar('TYPE', bound='Message')

    HEADER_SIZE = struct.calcsize('<HBBBBhHHiHH') # type: int
    MAGIC = struct.pack('<H', 0xDEAD) # type: bytes
    
    type = MessageType.TCP # type: MessageType # message type, e.g.: MessageType.TCP, MessageType.RPC
    mod = 0 # type: int # module id
    cmd = CommandType.UNKNOWN # type: CommandType # command type, e.g.: CommandType.STATUS_REPORT, 0 when response.

    channel = 0 # type: int # default to 0
    seq = 0 # type: int # default to 0
    rid = 0 # type: int # arm id
    monid = 0 # type: int # monitor id
    ret = 0 # type: int # default to 0

    body = None # type: typing.Any

    def __init__(self, body: typing.Any = None, type: MessageType = MessageType.TCP, mod: int = 0, cmd: CommandType = CommandType.UNKNOWN, channel: int = 0, seq: int = 0, rid: int = 0, monid: int = 0, ret: int = 0):
        self.type = type
        self.mod = mod
        self.cmd = cmd
        self.channel = channel
        self.seq = 0
        self.rid = rid
        self.monid = monid
        self.ret = ret
        self.body = body

        # override from json body, according to customer request
        if body:
            self.cmd = COMMAND_TYPE_MAPPING.get(body.get('type'), cmd)
            self.type = MessageType(body.get('msgMode', type))
            self.seq = int(body.get('msgSeq', seq))
            self.rid = int(body.get('params', {}).get('armID', rid))

    def Serialize(self) -> bytes:
        body = b''
        if self.body is not None:
            # TODO: fix
            # self.body['msgSeq'] = self.seq
            body = json.dumps(self.body).encode('utf-8')
        bodyChecksum = binascii.crc_hqx(body, 0)
        typeMode = int(self.type) | (self.mod << 3)
        monidRsv = (self.monid & 0x7)
        header = self.MAGIC + struct.pack('<BBBBhHHiH', typeMode, int(self.cmd), self.channel, self.seq, self.rid, len(body), monidRsv, self.ret, bodyChecksum)
        return header + struct.pack('<H', binascii.crc_hqx(header, 0)) + body

    @classmethod
    def Deserialize(cls: typing.Type[TYPE], data: bytes) -> typing.Tuple[typing.Optional[TYPE], bytes]:
        # first skip to the magic bytes
        head = data.find(cls.MAGIC)
        if head < 0:
            return None, b''
        data = data[head:]

        # wait for enough data to complete header
        if len(data) < cls.HEADER_SIZE:
            return None, data

        magic, typeMode, cmd, channel, seq, rid, size, monidRsv, ret, bodyChecksum, headerChecksum = struct.unpack('<HBBBBhHHiHH', data[:cls.HEADER_SIZE])
        header = struct.pack('<HBBBBhHHiH', magic, typeMode, cmd, channel, seq, rid, size, monidRsv, ret, bodyChecksum)
        if binascii.crc_hqx(header, 0) != headerChecksum:
            # if header checksum is not valid, skip the header
            log.error('message header checksum is invalid: %r', data)
            return None, data[cls.HEADER_SIZE:]

        # wait for enough data to complete header and body
        if len(data) < cls.HEADER_SIZE + size:
            return None, data

        body = data[cls.HEADER_SIZE:cls.HEADER_SIZE + size]
        if binascii.crc_hqx(body, 0) != bodyChecksum:
            # if body checksum is not valid, skip header and body
            log.error('message body checksum is invalid: %r', data)
            return None, data[cls.HEADER_SIZE + size:]
        data = data[cls.HEADER_SIZE + size:]

        try:
            body = json.loads(body.decode('utf-8'))
            seq = body.get('msgSeq', '') or seq
            # cmd in header is meaningless.  get body 'type' instead
            cmd = COMMAND_TYPE_MAPPING[body.get('type', 'unknown')]
            message = cls(
                type=MessageType(typeMode & 0x7),
                mod=(typeMode >> 3),
                cmd=CommandType(cmd),
                channel=channel,
                seq=seq,
                rid=rid,
                monid=(monidRsv & 0x7),
                ret=ret,
                body=body,
            )
            return message, data
        except Exception as e:
            log.exception('message body format is invalid: %s: %r', e, body)
        return None, data

    def __repr__(self) -> str:
        return '<Message(type=%r, mod=%r, cmd=%r, channel=%r, seq=%r, rid=%r, monid=%r, ret=%r, body=%r)>' % (
            self.type,
            self.mod,
            self.cmd,
            self.channel,
            self.seq,
            self.rid,
            self.monid,
            self.ret,
            self.body,
        )
