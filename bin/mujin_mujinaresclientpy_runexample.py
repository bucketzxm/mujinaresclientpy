#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import sys
import time
import argparse
import json
from mujinplc import plcmemory, plcserver, plcproductioncycle
from mujinaresclient import aresclient

log = logging.getLogger(__name__)
def _ConfigureLogging(logLevel=logging.DEBUG, outputStream=sys.stderr):
    handler = logging.StreamHandler(outputStream)
    try:
        import logutils.colorize
        handler = logutils.colorize.ColorizingStreamHandler(outputStream)
        handler.level_map[logging.DEBUG] = (None, 'green', False)
        handler.level_map[logging.INFO] = (None, None, False)
        handler.level_map[logging.WARNING] = (None, 'yellow', False)
        handler.level_map[logging.ERROR] = (None, 'red', False)
        handler.level_map[logging.CRITICAL] = ('white', 'magenta', True)
    except ImportError:
        pass
    handler.setFormatter(logging.Formatter('%(asctime)s %(name)s [%(levelname)s] [%(filename)s:%(lineno)s %(funcName)s] %(message)s'))
    handler.setLevel(logLevel)

    root = logging.getLogger()
    root.setLevel(logLevel)
    root.handlers = []
    root.addHandler(handler)

if __name__ == '__main__':
    _ConfigureLogging()

    parser = argparse.ArgumentParser(description='Run aresclient.')
    parser.add_argument('--config', action='store', type=str, dest='configfilename', required=True)
    parser.add_argument('--visionSimulationConfig', action='store', type=str, dest='visionSimulationConfigFilename', default='')
    options = parser.parse_args()

    """
    Config file example
    {
        "severip": "192.168.1.246",
        "port": 40011,
        "armID": 1,
        "warehouseID": "227375617572601865",
        "deviceType": 1
    }
    """
    config = {}
    visionSimulationConfig = {}
    with open(options.configfilename, 'r') as f:
        try:
            config = json.loads(f.read())
        except Exception as e:
            log.error('read config file %s error = %s'%(options.configfilename, e))
            sys.exit(1)

    visionSimulationConfig = None
    if options.visionSimulationConfigFilename:
        # use visionsimulation
        with open(options.visionSimulationConfigFilename, 'r') as f:
            try:
                visionSimulationConfig = json.loads(f.read())
            except Exception as e:
                log.error('read visoin simulation config file %s error = %s'%(options.visionSimulationConfig, e))
                sys.exit(1)

    configkeyset = set(('serverip', 'port', 'armID', 'warehouseID', 'deviceType'))
    if configkeyset != set(config.keys()) & configkeyset:
        log.error('config should include %s'%(configkeyset-set(config.keys())))
        sys.exit(1)

    memory = plcmemory.PLCMemory()
    logger = plcmemory.PLCMemoryLogger(memory, ignoredKeys=['commCounter'])
    
    # start production cycle
    productionCycle = None
    if True:
        productionCycle = plcproductioncycle.PLCProductionCycle(memory)
        productionCycle.Start()

    # start plc server
    server = plcserver.PLCServer(memory, 'tcp://*:5555')
    server.Start()

    useVisionSimulation = False
    if visionSimulationConfig:
        useVisionSimulation = True
        config.update(visionSimulationConfig)

    client = aresclient.Client(memory, (config['serverip'], int(config['port'])), useVisionSimulation=useVisionSimulation, **config)
    client.Start()

    # TODO(simon): handle signals
    time.sleep(10000)

    client.Stop()
    plcserver.Stop()
    if productionCycle:
        productionCycle.Stop()
