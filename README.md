# Install 
## install mujincontrollerclientpy and mujinplcpy.

**mujincontrollerclientpy is only used to do vision simulation for now.**

**mujinplcpy is needed to run production cycle and runner.**

### install mujinplcpy
1. git clone https://github.com/mujin/mujinplcpy
2. cd mujinplcpy
3. run `python3 setup.py install` to install mujinplcpy

### install mujincontrollerclientpy (option)
1. git clone https://github.com/mujin/mujincontrollerclientpy.git -b python3_simononsite
2. cd mujincontrollerclientpy
3. run `python3 setup.py install` to install mujincontrollerclientpy

### install pip requirements(option. if you installed mujincontrollerclientpy, you need to install the following package)
1. pip3 install numpy 
2. pip3 install zmq

### install mujinaresclientpy
1. pip3 install eventfd
2. git clone https://gitlab.com/bucketzxm/mujinaresclientpy.git -b add_read_config ( this branch hasn't been merged into master.)
3. cd mujinareclientpy
4. python3 setup.py install

# Run program
1. create config.json ( refer to template in mujinaresclientpy/config/config.json.template )
2. create visionsim.json ( this file is used to do vision simulation,  you don't need it if you are running real system.  refer to template in mujinaresclientpy/config/visionsim.json.template)
3. `python3 bin/mujin_mujinaresclientpy_runexample.py --config ./config.json --visionSimulationConfig ./visionsim.json`  (run with simulation. if you are running real system, don't add --vsionSimulationConfig option)

